package org.encyclosphere.encyclotorrent.api.aggregator;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.encyclosphere.encyclotorrent.api.misc.ExceptionHandler;
import org.encyclosphere.encyclotorrent.api.misc.Utilities;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static org.encyclosphere.encyclotorrent.api.misc.Utilities.log;
import static org.encyclosphere.encyclotorrent.api.misc.Utilities.makeRequest;

/**
 * Contains methods for managing {@link Aggregator}s.
 */
public class Aggregators {

    private static final ArrayList<Aggregator> localAggregators = new ArrayList<>();
    private static final ArrayList<String> remoteAggregators = new ArrayList<>();

    static {
        loadDefaultAggregators();
    }



    /**
     * Discovers more aggregators using a DHT-like algorithm.
     */
    public static void discoverAggregators() {
        contactedAggregators.clear();
        for(String aggregatorURL : remoteAggregators) contactAggregator(aggregatorURL);
        remoteAggregators.addAll(contactedAggregators);
    }

    private static final Set<String> contactedAggregators = new HashSet<>();

    private static void contactAggregator(String url) {
        if(url.isBlank()) return;
        log("Contacting " + url + "...");
        try {
            HttpResponse response = makeRequest(new HttpGet(url + "/aggregators"));
            if(response.getStatusLine().getStatusCode() == 200) {
                contactedAggregators.add(url);

                String[] aggregatorList = new String(response.getEntity().getContent().readAllBytes()).split("\n");
                log("Success. Got " + aggregatorList.length + " new aggregator(s)");
                for(String aggregator : aggregatorList) {
                    if(!contactedAggregators.contains(aggregator)) contactAggregator(aggregator);
                }
            } else {
                log("Failed to contact " + url + ": " + response.getStatusLine().getStatusCode() + " " + response.getStatusLine().getReasonPhrase());
                remoteAggregators.remove(url);
            }
        } catch(Exception e) {
            Utilities.logException("Failed to contact " + url, e);
            remoteAggregators.remove(url);
        }
    }

    /**
     * Checks if an aggregator is online.
     * @param aggregatorURL The URL of the aggregator, without a trailing slash
     * @return Whether the aggregator is online
     */
    public static boolean isOnline(String aggregatorURL) {
        try {
            HttpResponse response = makeRequest(new HttpGet(aggregatorURL + "/info"));
            if(response.getStatusLine().getStatusCode() == 200) return true;
        } catch(IOException e) {
            return false;
        }
        return false;
    }

    /**
     * Gets information about an aggregator.
     * @param aggregatorURL The URL of the aggregator, without a trailing slash
     * @return Information about the aggregator in the form of an {@link AggregatorInfo} object,
     *         or {@code null} if the aggregator couldn't be reached
     */
    public static AggregatorInfo getInfo(String aggregatorURL) {
        try {
            HttpResponse response = makeRequest(new HttpGet(aggregatorURL + "/info"));
            if(response.getStatusLine().getStatusCode() != 200) return null;
            return new AggregatorInfo(new JSONObject(new String(response.getEntity().getContent().readAllBytes())));
        } catch(IOException e) {
            return null;
        }
    }

    /**
     * Adds an aggregator to the list of known aggregators.
     * @param aggregatorURL The URL of an aggregator, without a trailing slash
     */
    public static void registerAggregator(String aggregatorURL) {
        if(!remoteAggregators.contains(aggregatorURL)) remoteAggregators.add(aggregatorURL);
    }

    /**
     * Removes an aggregator from the list of known aggregators.
     * @param url The URL of the aggregator to remove
     */
    public static void deregisterAggregator(String url) {
        remoteAggregators.remove(url);
    }

    /**
     * Moves an aggregator down in the list,
     * meaning it's less likely to be contacted when downloading a file.
     * @param url The URL of the aggregator to demote
     */
    public static void demoteAggregator(String url) {
        remoteAggregators.remove(url);
        remoteAggregators.add(url);
    }

    // Used internally by the Aggregator constructor
    static void registerLocalAggregator(Aggregator aggregator) {
        localAggregators.add(aggregator);
    }

    // Used internally by Aggregator#destroy()
    static void deregisterLocalAggregator(Aggregator aggregator) {
        localAggregators.remove(aggregator);
    }

    // Used internally by the aggregator servlets
    public static Aggregator getLocalAggregator(int port) {
        if(localAggregators.size() == 1) return localAggregators.get(0);
        for(Aggregator aggregator : localAggregators) if(aggregator.getConfig().getPort() == port) return aggregator;
        return null;
    }

    /**
     * @return An {@link ArrayList<String>} of URLs of known aggregators
     */
    public static ArrayList<String> getAggregators() {
        return remoteAggregators;
    }

    /**
     * Resets the list of known aggregators.
     */
    public static void resetAggregatorList() {
        remoteAggregators.clear();
        loadDefaultAggregators();
    }

    private static void loadDefaultAggregators() {
        try {
            InputStream aggregatorStream = Aggregators.class.getResourceAsStream("/aggregators.txt");
            if(aggregatorStream == null) throw new IOException("Failed to read aggregators.txt");
            String[] contentsSplit = new String(aggregatorStream.readAllBytes()).split("\n");
            ArrayList<String> hardCodedAggregators = new ArrayList<>(Arrays.stream(contentsSplit).filter(line -> !line.strip().startsWith("#")).toList());
            remoteAggregators.addAll(hardCodedAggregators);
        } catch(IOException e) {
            ExceptionHandler.getExceptionHandler().accept(e);
        }
    }

}
