package org.encyclosphere.encyclotorrent.api.aggregator.servlets.download;

import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.encyclosphere.encyclotorrent.api.aggregator.Aggregator;
import org.encyclosphere.encyclotorrent.api.aggregator.Aggregators;
import org.encyclosphere.encyclotorrent.api.misc.Utilities;
import org.encyclosphere.encyclotorrent.api.zwi.ZWIFile;
import org.encyclosphere.encyclotorrent.api.zwi.ZWITorrent;

import java.io.IOException;

public class TorrentDownloadServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String id = request.getParameter("id");
        if(id == null) {
            Utilities.sendError(response, 400, "Missing ID");
            return;
        }

        Aggregator aggregator = Aggregators.getLocalAggregator(request.getLocalPort());
        assert aggregator != null;

        ZWIFile zwiFile = aggregator.getDatabase().getZWIFile(id);
        if(zwiFile == null) {
            Utilities.sendError(response, 404, "Couldn't find an article with the given ID in the database");
            return;
        }

        ZWITorrent torrent = zwiFile.getTorrent(aggregator.getTorrentTrackers());
        response.setContentType("application/x-bittorrent");
        response.setHeader("content-disposition", "attachment; filename=\"" + torrent.getFilename() + "\"");
        response.getOutputStream().write(torrent.getTorrentBytes());
        response.getOutputStream().close();
    }

}
