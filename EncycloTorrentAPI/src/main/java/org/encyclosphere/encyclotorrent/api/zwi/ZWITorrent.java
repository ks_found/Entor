package org.encyclosphere.encyclotorrent.api.zwi;

/**
 * A torrent of a ZWI file.
 */
@SuppressWarnings("ClassCanBeRecord")
public class ZWITorrent {

    private final String filename;
    private final byte[] torrentBytes;

    public ZWITorrent(String filename, byte[] torrentBytes) {
        this.filename = filename;
        this.torrentBytes = torrentBytes;
    }

    public String getFilename() {
        return filename;
    }

    public byte[] getTorrentBytes() {
        return torrentBytes;
    }

}
