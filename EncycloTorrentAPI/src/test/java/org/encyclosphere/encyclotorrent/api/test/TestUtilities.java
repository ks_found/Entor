package org.encyclosphere.encyclotorrent.api.test;

import org.encyclosphere.encyclotorrent.api.misc.Utilities;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class TestUtilities {

    static final boolean DELETE_TEMP_ON_TEST_FINISH = false;

    static final Path ARTICLE_PATH = Path.of("src/test/resources/articles"),
            TORRENT_PATH = Path.of("src/test/resources/torrents"),
            FIRST_ZWI_PATH = ARTICLE_PATH.resolve("First_test_article.zwi"),
            SECOND_ZWI_PATH = ARTICLE_PATH.resolve("Second_test_article.zwi"),
            THIRD_ZWI_PATH = ARTICLE_PATH.resolve("Third_test_article.zwi"),
            FIRST_TORRENT_PATH = TORRENT_PATH.resolve("ZXhhbXBsZS5jb20vd2lraS9UZXN0X2FydGljbGU=@1644624000.torrent"),
            SECOND_TORRENT_PATH = TORRENT_PATH.resolve("ZXhhbXBsZS5jb20vd2lraS9TZWNvbmRfdGVzdF9hcnRpY2xl@1644624000.torrent"),
            THIRD_TORRENT_PATH = TORRENT_PATH.resolve("ZXhhbXBsZS5jb20vd2lraS9UaGlyZF90ZXN0X2FydGljbGU=@1644624000.torrent");

    static boolean hashesMatch(byte[] one, Path other) throws IOException {
        return Utilities.getSHA1Hash(one).equals(Utilities.getSHA1Hash(Files.readAllBytes(other)));
    }

}
