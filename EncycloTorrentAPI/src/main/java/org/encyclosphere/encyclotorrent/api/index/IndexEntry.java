package org.encyclosphere.encyclotorrent.api.index;

import org.encyclosphere.encyclotorrent.api.misc.Utilities;

/**
 * Contains information about a ZWI file.
 */
public class IndexEntry {

    private final String id, url, title;

    private final long filesize, timestamp;

    public IndexEntry(String sourceString) {
        String[] split = sourceString.split("(,)(?=(?:[^\"]|\"[^\"]*\")*$)"); // Matches all commas outside double quotes. From https://stackoverflow.com/a/632552/5905216
        this.id = split[0];
        this.url = split[1];
        this.title = split[2].substring(1, split[2].length() - 1); // Substring removes quotes
        this.filesize = Long.parseLong(split[3]);
        this.timestamp = Long.parseLong(split[4]);
    }

    public String getID() {
        return id;
    }

    public String getFilename() {
        return id + ".zwi";
    }

    public String getPath() {
        return Utilities.getPartialPath(id);
    }

    public String getPathInDatabase() {
        return getPath() + "/" + getFilename();
    }

    public String getURL() {
        return url;
    }

    public String getTitle() {
        return title;
    }

    public long getFileSize() {
        return filesize;
    }

    public long getTimestamp() {
        return timestamp;
    }

    @Override
    public String toString() {
        StringBuilder indexLineBuilder = new StringBuilder();
        indexLineBuilder
                .append(id).append(",")                                 // id
                .append("\"").append(url).append("\"").append(",")      // url
                .append("\"").append(title).append("\"").append(",")    // title
                .append(filesize).append(",")                           // filesize
                .append(timestamp);                                     // timestamp
        return indexLineBuilder.toString();
    }

}
