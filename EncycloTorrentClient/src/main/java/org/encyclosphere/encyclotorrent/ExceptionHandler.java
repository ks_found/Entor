package org.encyclosphere.encyclotorrent;

import org.apache.http.NoHttpResponseException;
import org.apache.http.conn.HttpHostConnectException;
import org.encyclosphere.encyclotorrent.api.misc.Utilities;
import org.fusesource.jansi.AnsiConsole;

import java.io.IOException;
import java.nio.file.InvalidPathException;

import static org.fusesource.jansi.Ansi.Color.RED;
import static org.fusesource.jansi.Ansi.ansi;

public class ExceptionHandler {

    public static void handleException(Throwable e) {
        String stackTrace = Utilities.getStackTrace(e);
        String message = "An error occurred.";
        if(e instanceof HttpHostConnectException hhce) message = "Failed to connect to " + hhce.getHost().toURI() + ".";
        if(e instanceof NoHttpResponseException) message = "Failed to connect to server.";
        else if(e instanceof IOException) message = "An I/O error occurred.";
        else if(e instanceof InvalidPathException) message = "Invalid path.";
        System.out.println(ansi().fg(RED).a(message + " Details:\n" + stackTrace).reset());
    }
}
