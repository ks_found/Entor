package org.encyclosphere.encyclotorrent.api.misc;

/**
 * Contains language constants.
 * @see <a href="https://www.sitepoint.com/iso-2-letter-language-codes/">Source</a>
 */
public enum Language {
    ENGLISH("en"),
    ABKHAZIAN("ab"),
    AFAR("aa"),
    AFRIKAANS("af"),
    ALBANIAN("sq"),
    AMHARIC("am"),
    ARABIC("ar"),
    ARMENIAN("hy"),
    ASSAMESE("as"),
    AYMARA("ay"),
    AZERBAIJANI("az"),
    BASHKIR("ba"),
    BASQUE("eu"),
    BENGALI_BANGLA("bn", "Bengali/Bangla"),
    BHUTANI("dz"),
    BIHARI("bh"),
    BISLAMA("bi"),
    BRETON("br"),
    BULGARIAN("bg"),
    BURMESE("my"),
    BYELORUSSIAN("be"),
    CAMBODIAN("km"),
    CATALAN("ca"),
    CHINESE("zh"),
    CORSICAN("co"),
    CROATIAN("hr"),
    CZECH("cs"),
    DANISH("da"),
    DUTCH("nl"),
    ESPERANTO("eo"),
    ESTONIAN("et"),
    FAEROESE("fo"),
    FIJI("fj"),
    FINNISH("fi"),
    FRENCH("fr"),
    FRISIAN("fy"),
    GAELIC("gd"),
    GALICIAN("gl"),
    GEORGIAN("ka"),
    GERMAN("de"),
    GREEK("el"),
    GREENLANDIC("kl"),
    GUARANI("gn"),
    GUJARATI("gu"),
    HAUSA("ha"),
    HEBREW("iw"),
    HINDI("hi"),
    HUNGARIAN("hu"),
    ICELANDIC("is"),
    INDONESIAN("in"),
    INTERLINGUA("ia"),
    INTERLINGUE("ie"),
    INUPIAK("ik"),
    IRISH("ga"),
    ITALIAN("it"),
    JAPANESE("ja"),
    JAVANESE("jw"),
    KANNADA("kn"),
    KASHMIRI("ks"),
    KAZAKH("kk"),
    KINYARWANDA("rw"),
    KIRGHIZ("ky"),
    KIRUNDI("rn"),
    KOREAN("ko"),
    KURDISH("ku"),
    LAOTHIAN("lo"),
    LATIN("la"),
    LATVIAN_LETTISH("lv", "Latvian/Lettish"),
    LINGALA("ln"),
    LITHUANIAN("lt"),
    MACEDONIAN("mk"),
    MALAGASY("mg"),
    MALAY("ms"),
    MALAYALAM("ml"),
    MALTESE("mt"),
    MAORI("mi"),
    MARATHI("mr"),
    MOLDAVIAN("mo"),
    MONGOLIAN("mn"),
    NAURU("na"),
    NEPALI("ne"),
    NORWEGIAN("no"),
    OCCITAN("oc"),
    ORIYA("or"),
    OROMO_AFAN("om", "Oromo/Afan"),
    PASHTO_PUSHTO("ps", "Pashto/Pushto"),
    PERSIAN("fa"),
    POLISH("pl"),
    PORTUGUESE("pt"),
    PUNJABI("pa"),
    QUECHUA("qu"),
    RHAETO_ROMANCE("rm", "Rhaeto-Romance"),
    ROMANIAN("ro"),
    RUSSIAN("ru"),
    SAMOAN("sm"),
    SANGRO("sg"),
    SANSKRIT("sa"),
    SERBIAN("sr"),
    SERBO_CROATIAN("sh", "Serbo-Croatian"),
    SESOTHO("st"),
    SETSWANA("tn"),
    SHONA("sn"),
    SINDHI("sd"),
    SINGHALESE("si"),
    SISWATI("ss"),
    SLOVAK("sk"),
    SLOVENIAN("sl"),
    SOMALI("so"),
    SPANISH("es"),
    SUDANESE("su"),
    SWAHILI("sw"),
    SWEDISH("sv"),
    TAGALOG("tl"),
    TAJIK("tg"),
    TAMIL("ta"),
    TATAR("tt"),
    TEGULU("te"),
    THAI("th"),
    TIBETAN("bo"),
    TIGRINYA("ti"),
    TONGA("to"),
    TSONGA("ts"),
    TURKISH("tr"),
    TURKMEN("tk"),
    TWI("tw"),
    UKRAINIAN("uk"),
    URDU("ur"),
    UZBEK("uz"),
    VIETNAMESE("vi"),
    VOLAPUK("vo"),
    WELSH("cy"),
    WOLOF("wo"),
    XHOSA("xh"),
    YIDDISH("ji"),
    YORUBA("yo"),
    ZULU("zu");



    private final String code, name;

    Language(String code) {
        this.code = code;
        String name = this.toString();
        this.name = name.charAt(0) + name.substring(1).toLowerCase();
    }

    Language(String code, String name) {
        this.code = code;
        this.name = name;
    }



    /**
     * @return This {@link Language}'s 2-letter code
     */
    public String getCode() {
        return code;
    }

    /**
     * @return This {@link Language}'s human-readable name
     */
    public String getName() {
        return name;
    }



    /**
     * Gets a {@link Language} object from a 2-letter code.
     * @param code A 2-letter language code
     * @return The corresponding {@link Language} object
     */
    public static Language fromCode(String code) {
        for(Language lang : values()) if(lang.code.equals(code)) return lang;
        return null;
    }

    /**
     * Checks if a {@link String} is a valid language code.
     * @param code The {@code String} to check
     * @return Whether the {@code String} is a valid language code
     */
    public static boolean isValid(String code) {
        for(Language lang : values()) if(lang.code.equals(code)) return true;
        return false;
    }

}
