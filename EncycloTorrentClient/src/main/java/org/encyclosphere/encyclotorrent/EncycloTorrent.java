package org.encyclosphere.encyclotorrent;

import org.encyclosphere.encyclotorrent.api.Downloader;
import org.encyclosphere.encyclotorrent.api.Searcher;
import org.encyclosphere.encyclotorrent.api.Uploader;
import org.encyclosphere.encyclotorrent.api.aggregator.Aggregator;
import org.encyclosphere.encyclotorrent.api.aggregator.AggregatorConfiguration;
import org.encyclosphere.encyclotorrent.api.aggregator.AggregatorInfo;
import org.encyclosphere.encyclotorrent.api.aggregator.Aggregators;
import org.encyclosphere.encyclotorrent.api.misc.TorrentClient;
import org.encyclosphere.encyclotorrent.api.misc.Utilities;
import org.encyclosphere.encyclotorrent.api.zwi.ZWIDatabase;
import org.encyclosphere.encyclotorrent.api.zwi.ZWIFile;
import org.encyclosphere.encyclotorrent.api.zwi.ZWIMetadata;
import org.encyclosphere.encyclotorrent.api.zwi.ZWITorrent;
import org.fusesource.jansi.Ansi;
import org.fusesource.jansi.AnsiConsole;

import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.text.CharacterIterator;
import java.text.StringCharacterIterator;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.LogManager;

import static org.encyclosphere.encyclotorrent.api.misc.Utilities.listFiles;
import static org.fusesource.jansi.Ansi.ansi;

public class EncycloTorrent {

    static final Scanner scanner = new Scanner(System.in);

    static final Thread shutdownHookThread = new Thread(() -> {
        try {
            Configuration.save();
        } catch(IOException e) {
            ExceptionHandler.handleException(e);
        }
    });

    public static void main(String[] args) throws Exception {

        // Initial setup
        LogManager.getLogManager().reset();
        AnsiConsole.systemInstall();
        TorrentClient.init();
        Configuration.load();
        Thread.currentThread().setUncaughtExceptionHandler((thread, e) -> ExceptionHandler.handleException(e));
        Runtime.getRuntime().addShutdownHook(shutdownHookThread);

        // If there are no arguments, print usage and exit
        if(args.length == 0) printUsageAndExit(0);

        // If the "verbose" argument is present, enable debug logging
        boolean verbose = args[args.length - 1].equals("verbose");
        if(verbose) {
            Utilities.setLoggingEnabled(true);
            args = Arrays.copyOf(args, args.length - 1);
        }

        switch(args[0]) {

            // enc help
            case "help", "--help", "-help", "-h" -> {
                if(args.length != 1) invalidArgs();
                printUsageAndExit(0);
            }

            // enc search <keywords>
            case "search" -> {
                if(args.length != 2) invalidArgs();
                ArrayList<ZWIDatabase.SearchResult> results = Searcher.search(args[1], 10);
                if(results == null) error("Unable to get results from any aggregators.");
                assert results != null;
                if(results.size() == 0) println("No results for \"" + args[1] + "\".");
                else println((results.size() == 10 ? "First " : "") + results.size() + " results for \"" + args[1] + "\":\n");
                for(ZWIDatabase.SearchResult result : results) displayResult(result);
            }

            // enc upload article.zwi
            case "upload" -> {
                if(!(args.length == 2 || args.length == 3)) invalidArgs();

                String aggregatorURL = null;
                if(args.length == 3) aggregatorURL = args[2];
                ZWIFile zwiFile = new ZWIFile(Path.of(args[1]));
                String filename = zwiFile.getLocation().getFileName().toString();
                String publisher = zwiFile.getMetadata().getPublisher();
                int numAggregators = aggregatorURL == null ? Aggregators.getAggregators().size() : 1;

                println(ansi()
                        .fg(Ansi.Color.YELLOW)
                        .a("Uploading ")
                        .bold()
                        .a(filename)
                        .boldOff()
                        .a(" to " + numAggregators + " aggregator" + (numAggregators == 1 ? "" : "s") + "...")
                        .reset());

                int successfulUploads = 0;
                if(aggregatorURL == null) successfulUploads = Uploader.uploadToAggregators(zwiFile, Configuration.getPasswords(publisher));
                else {
                    String password = null;
                    Configuration.PasswordEntry entry = Configuration.getPassword(aggregatorURL, publisher);
                    if(entry != null) password = entry.password();
                    boolean success = Uploader.uploadToAggregator(zwiFile, aggregatorURL, password);
                    if(success) successfulUploads = 1;
                }

                if(successfulUploads > 0) {
                    println(ansi()
                            .fg(Ansi.Color.GREEN)
                            .a("Uploaded ")
                            .bold()
                            .a(filename)
                            .boldOff()
                            .a(" to " + successfulUploads + "/" + numAggregators + " aggregators.")
                            .reset());
                } else error("Unable to upload to any aggregators.");
            }

            // enc info enc://<ID>
            case "info" -> {
                if(args[1].equals("raw")) {
                    if(args.length != 3) invalidArgs();

                    String id = id(args[2]);
                    String rawMetadata = Downloader.getRawMetadata(id);
                    if(rawMetadata == null) error("Couldn't find that article.");
                    println(rawMetadata);
                } else {
                    if(args.length != 2) invalidArgs();

                    String id = id(args[1]);

                    ZWIMetadata metadata = Downloader.getMetadata(id);
                    if(metadata == null) error("Couldn't find that article.");
                    assert metadata != null;

                    bold(id);
                    info("Title", metadata.getTitle());
                    info("Language", metadata.getLanguageCode());
                    info("From", metadata.getPublisher());
                    info("Last modified", metadata.getLastModified().format(DateTimeFormatter.ofLocalizedDate(FormatStyle.FULL)));
                    info("Created", metadata.getTimeCreated().format(DateTimeFormatter.ofLocalizedDate(FormatStyle.FULL)));
                    info("Categories", metadata.getCategories().toString());
                    info("Description", metadata.getDescription());
                    info("Comment", metadata.getComment());
                    info("License", metadata.getLicense());
                }
            }

            // enc download
            case "download" -> {
                if(args.length < 2) invalidArgs();

                switch(args[1]) {

                    // enc download enc://<ID>
                    default -> {
                        if(!(args.length == 2 || args.length == 3)) invalidArgs();

                        // Get parameters
                        String id = id(args[1]);
                        Path targetDirectory = (args.length == 3 ? Path.of(args[2].strip()) : Path.of("."));

                        // Show information about the article; confirm download
                        confirmDownload(id, "Download this article?");

                        // Download the ZWI
                        yellow("Downloading...");
                        ZWIFile zwiFile = Downloader.downloadZWIFile(id, targetDirectory);
                        if(zwiFile != null) green("Download complete.");
                        else {
                            red("Download failed.");
                            exit(1);
                        }
                    }

                    // enc download torrent enc://<ID>
                    case "torrent" -> {
                        if(!(args.length == 3 || args.length == 4)) invalidArgs();

                        // Get parameters
                        String id = id(args[2]);
                        Path targetDirectory = (args.length == 4 ? Path.of(args[3]) : Path.of("."));

                        // Show information about the article; confirm download
                        confirmDownload(id, "Download a torrent of this article?");

                        // Download the torrent
                        ZWITorrent torrent = Downloader.downloadTorrent(id);
                        assert torrent != null;
                        Files.write(targetDirectory.resolve(torrent.getFilename()), torrent.getTorrentBytes(), StandardOpenOption.CREATE_NEW);
                    }

                    // enc download withTorrent <torrent>
                    case "withTorrent" -> {
                        if(!(args.length == 3 || args.length == 4)) invalidArgs();

                        // Get parameters
                        Path torrentFile = Path.of(args[2]);
                        Path targetDirectory = (args.length == 4 ? Path.of(args[3]) : Path.of("."));

                        // Download the file
                        Downloader.downloadZWIFileUsingTorrentFile(torrentFile, targetDirectory);
                    }
                }
            }

            // enc pull <aggregator URL> <database dir>
            // enc sync <aggregator URL> <database dir>
            case "sync", "pull" -> {
                if(args.length != 3) invalidArgs();

                String syncTypeArg = args[0].strip();
                if(!(syncTypeArg.equals("sync") || syncTypeArg.equals("pull"))) invalidArgs();
                boolean continuous = syncTypeArg.equals("sync");

                String source = url(args[1]);
                String target = args[2].strip();

                Path targetPath = Path.of(target);
                boolean localFolderExists = Files.exists(targetPath);

                ZWIDatabase database = new ZWIDatabase(targetPath);
                if(localFolderExists) database.updateInfo();

                info("Source", source);
                info("Target", database.getRoot().toAbsolutePath());
                if(localFolderExists) info("Number of articles in local database", database.getArticleCount());
                AggregatorInfo aggregatorInfo = Aggregators.getInfo(source);
                if(aggregatorInfo == null) error("Unable to connect to aggregator.");
                assert aggregatorInfo != null;
                info("Number of articles in remote database", aggregatorInfo.getArticleCount());
                if(continuous) info("Sync mode", "Continuous (sync every 5 minutes)");

                confirm("Start sync?", false);
                if(continuous) {
                    render("Syncing @|bold " + database.getRoot().getFileName() + "|@ with @|bold " + args[2] + "|@ every 5 minutes.");
                    render("To stop syncing, press @|bold Ctrl+C|@.");

                    ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
                    scheduler.scheduleAtFixedRate(() -> {
                        yellow("Syncing...");
                        try {
                            boolean success = database.syncWith(source, true);
                            println(success);
                            if(success) green("Sync complete.");
                            else red("Sync failed.");
                        } catch(IOException e) {
                            ExceptionHandler.handleException(e);
                        }
                        println("Waiting 5 minutes...");
                    }, 0, 5, TimeUnit.MINUTES);
                } else {
                    // Equivalent to render("@|yellow Syncing @|bold <target>|@ with @|bold <source>|@...|@"), if nesting codes was possible
                    println(ansi().fg(Ansi.Color.YELLOW).a("Syncing ").bold().a(database.getRoot().getFileName().toString()).boldOff().a(" with ").bold().a(source).boldOff().a("...").reset());

                    boolean success = database.syncWith(source, true);
                    if(success) green("Sync complete.");
                    else error("Sync failed.");
                }
            }

            // enc pass
            case "pass" -> {
                if(args.length < 2) invalidArgs();
                switch(args[1]) {

                    // enc pass add <aggregator URL> <publisher> <password>
                    case "add" -> {
                        if(args.length != 5) invalidArgs();
                        Configuration.addPassword(new Configuration.PasswordEntry(args[2], args[3], args[4]));
                        green("Password added.");
                    }

                    // enc pass get <aggregator URL> <publisher>
                    case "get" -> {
                        if(args.length != 4) invalidArgs();
                        Configuration.PasswordEntry entry = Configuration.getPassword(args[2], args[3]);
                        if(entry != null) println(entry.password());
                        else error("Couldn't find that password.");
                    }

                    // enc pass remove <aggregator URL> <publisher>
                    case "remove" -> {
                        if(args.length != 4) invalidArgs();
                        render("Removing password for aggregator: @|bold " + args[2] + "|@, publisher: @|bold " + args[3] + "|@");
                        confirm("This cannot be undone. Continue?", true);
                        Configuration.PasswordEntry entry = Configuration.getPassword(args[2], args[3]);
                        if(entry != null) {
                            Configuration.removePassword(entry);
                            green("Password removed.");
                        } else error("Couldn't find that password.");
                    }

                    // enc pass list
                    // enc pass list <aggregator URL>
                    case "list" -> {
                        if(args.length == 2) {
                            if(Configuration.getPasswords().size() == 0) {
                                println("No passwords.");
                                break;
                            }
                            // TODO Improve display of passwords; obfuscate passwords?
                            for(Configuration.PasswordEntry entry : Configuration.getPasswords()) {
                                info("Aggregator", entry.aggregatorURL());
                                info("Publisher", entry.publisher());
                                info("Password", entry.password());
                                println();
                            }
                        } else if(args.length == 3) {
                            String aggregatorURL = url(args[2]);
                            boolean passwordsShown = false;
                            for(Configuration.PasswordEntry entry : Configuration.getPasswords()) {
                                if(entry.aggregatorURL().equals(aggregatorURL)) {
                                    passwordsShown = true;
                                    info("Publisher", entry.publisher());
                                    info("Password", entry.password());
                                    println();
                                }
                            }
                            if(!passwordsShown) render("No passwords for @|bold " + aggregatorURL + "|@.");
                        } else invalidArgs();
                    }

                    // enc pass clear
                    case "clear" -> {
                        yellow("WARNING: All aggregator passwords will be deleted. This cannot be undone.");
                        confirm("Continue?", false);
                        Configuration.getPasswords().clear();
                        green("All passwords removed.");
                    }

                }
            }

            // enc aggregator
            case "aggregator" -> {
                if(args.length < 2) invalidArgs();
                switch(args[1]) {

                    // enc aggregator create
                    case "create" -> {

                        // enc aggregator create withDatabase <database dir>
                        if(args[2].equals("withDatabase")) {
                            if(args.length != 4) invalidArgs();

                            Path databaseDir = Path.of(args[3]).toAbsolutePath();
                            if(Files.notExists(databaseDir)) error("Couldn't find a database at that location.");
                            if(isAggregatorFolder(databaseDir)) error("An aggregator already exists at that location.");

                            println(ansi().fg(Ansi.Color.YELLOW).bold().a("WARNING: ").boldOff().a("This command will make irreversible changes to the database:"));
                            println(ansi().bold().a(databaseDir.toAbsolutePath()).boldOff());
                            println(ansi().a("Ensure you've made a backup of the database before continuing.").reset());

                            confirm("Continue?", false);

                            // Move all files and folders to the "db" folder
                            Path newDatabasePath = databaseDir.resolve("db");
                            Files.createDirectories(newDatabasePath);
                            for(Path file : listFiles(databaseDir)) {
                                if(!file.getFileName().toString().strip().equals("db")) Files.move(file, newDatabasePath.resolve(file.getFileName().toString()));
                            }

                            // Make sure all ZWI files are in the correct directories; build index files
                            ZWIDatabase database = new ZWIDatabase(newDatabasePath);
                            database.migrate(true);

                            // Create the config file, aggregator.json
                            Path configPath = databaseDir.resolve("aggregator.json");
                            AggregatorConfiguration.createConfigFile(configPath);
                            green("Aggregator created successfully.");
                            render("To start it, run this command: @|bold enc aggregator start " + databaseDir.getFileName().toString() + "|@");

                        // enc aggregator create <directory>
                        } else {
                            if(args.length != 3) invalidArgs();

                            Path rootDir = Path.of(args[2]).toAbsolutePath();
                            if(Files.exists(rootDir)) throw new FileAlreadyExistsException(rootDir.toAbsolutePath().toString());

                            Files.createDirectories(rootDir.resolve("db"));
                            AggregatorConfiguration.createConfigFile(rootDir.resolve("aggregator.json"));

                            green("Aggregator created successfully.");
                            render("To start it, run this command: @|bold enc aggregator start " + rootDir.getFileName().toString() + "|@");
                        }
                    }

                    // enc aggregator start <directory>
                    case "start" -> {
                        if(args.length != 3) invalidArgs();
                        Aggregator aggregator = new Aggregator(AggregatorConfiguration.load(Path.of(args[2])));
                        AggregatorConfiguration config = aggregator.getConfig();
                        bold(config.getName());
                        println(config.getDescription());
                        info("Listening on", aggregator.getURL());
                        info("Number of articles", aggregator.getInfo().getArticleCount());
                        info("Database size", humanReadableByteCountSI(aggregator.getInfo().getDatabaseSize()));
                        render("Server is starting. To stop it, press @|bold Ctrl+C|@.");
                        aggregator.start();
                        green("Server started.");
                    }

                    // enc aggregator info <aggregator URL>
                    case "info" -> {
                        String url = url(args[2]);
                        AggregatorInfo info = Aggregators.getInfo(url);
                        if(info == null) error("Unable to connect to aggregator.");
                        assert info != null;
                        showAggregatorInfo(info);
                    }

                    // enc aggregator add <aggregator URL>
                    case "add" -> {
                        if(args.length != 3) invalidArgs();
                        Aggregators.registerAggregator(url(args[2]));
                        green("Aggregator added.");
                    }

                    // enc aggregator list
                    case "list" -> {
                        if(args.length == 2) {
                            for(String url : Aggregators.getAggregators()) {
                                if(Aggregators.isOnline(url)) green("⬤ Online: " + url);
                                else red("⬤ Offline: " + url);
                            }
                        } else if(args.length == 3) {
                            yellow("WARNING: The list of aggregators will be reset. This cannot be undone.");
                            confirm("Continue?", false);
                            Aggregators.resetAggregatorList();
                            green("List reset.");
                        } else invalidArgs();
                    }

                    // enc aggregator remove <aggregator URL>
                    case "remove" -> {
                        if(args.length != 3) invalidArgs();
                        Aggregators.deregisterAggregator(url(args[2]));
                        green("Aggregator removed.");
                    }

                    // enc aggregator discover
                    case "discover" -> Aggregators.discoverAggregators();

                    // enc aggregator someInvalidOption
                    default -> invalidArgs();
                }
            }

            // enc database
            case "database" -> {
                if(args.length < 3) invalidArgs();

                Path folder = Path.of(args[1]);
                ZWIDatabase database;
                if(Files.notExists(folder) || !(Files.isDirectory(folder))) error("Couldn't find a database at: " + folder.toAbsolutePath());
                if(isAggregatorFolder(folder)) database = new ZWIDatabase(folder.resolve("db"));
                else database = new ZWIDatabase(folder);

                switch(args[2]) {

                    case "search" -> {
                        ArrayList<ZWIDatabase.SearchResult> results = database.search(args[3], 10, ZWIDatabase.SEARCH_ALL_FIELDS, null, null);
                        if(results.size() == 0) println("No results for \"" + args[3] + "\".");
                        else println((results.size() == 10 ? "First " : "") + results.size() + " results for \"" + args[3] + "\":\n");
                        for(ZWIDatabase.SearchResult result : results) displayResult(result);
                    }

                    case "add" -> {
                        if(!ZWIFile.isValid(new ZWIFile(Path.of(args[3])))) error("Invalid ZWI file.");
                        database.addFile(Path.of(args[3]), true, true);
                        green("File added.");
                    }

                    case "trash" -> {
                        ZWIFile zwiFile = database.getZWIFile(id(args[3]));
                        if(zwiFile == null) error("Couldn't find that article.");
                        assert zwiFile != null;
                        ZWIMetadata metadata = zwiFile.getMetadata();
                        info("Title", metadata.getTitle());
                        info("Description", truncate(metadata.getDescription()));
                        info("From", metadata.getPublisher());
                        confirm("Move this file to the trash?", true);
                        database.trashFile(zwiFile);
                        green("Article moved to trash.");
                    }

                    case "emptyTrash" -> {
                        yellow("WARNING: All files in the trash will be permanently deleted. You can't undo this action.");
                        confirm("Continue?", false);
                        database.emptyTrash();
                        green("Trash emptied.");
                    }

                    case "buildIndexes" -> {
                        database.buildIndexFiles(true);
                        database.buildLuceneIndex(true);
                        green("Done building indexes.");
                    }

                    default -> invalidArgs();

                }
            }

            // enc reset
            case "reset" -> {
                yellow("WARNING: This will delete the .encyclotorrent directory, resetting the program. This cannot be undone.");
                confirm("Continue?", false);
                Utilities.deleteDirectory(Configuration.CONFIG_DIR);
                Runtime.getRuntime().removeShutdownHook(shutdownHookThread);
            }

            // enc someInvalidOption
            default -> invalidArgs();
        }
    }

    static void println(Object object) {
        System.out.println(object);
    }

    static void println() {
        System.out.println();
    }

    static void render(String text) {
        println(ansi().render(text));
    }

    static String id(String arg) {
        String id = arg.strip();
        if(!id.startsWith("enc://")) error("Invalid article ID.");
        return id;
    }

    static String url(String arg) {
        arg = arg.strip();
        if(arg.endsWith("/")) return arg.substring(arg.length() - 1); // Remove trailing slash from URL
        else return arg;
    }

    static void red(String text) {
        println(ansi().fg(Ansi.Color.RED).a(text).reset());
    }

    static void yellow(String text) {
        println(ansi().fg(Ansi.Color.YELLOW).a(text).reset());
    }

    static void green(String text) {
        println(ansi().fg(Ansi.Color.GREEN).a(text).reset());
    }

    static void bold(String text) {
        println(ansi().bold().a(text).reset());
    }

    static void info(String key, Object value) {
        println(ansi().bold().a(key + ": ").boldOff().a(value).reset());
    }

    static void confirm(String message, boolean yesDefault) {
        System.out.print(message + " " + (yesDefault ? "[Y/n] " : "[y/N] "));
        String input = scanner.nextLine();
        if(yesDefault) {
            if(input.equalsIgnoreCase("n")) {
                red("Aborted.");
                exit(0);
            }
        } else {
            if(!input.equalsIgnoreCase("y")) {
                red("Aborted.");
                exit(0);
            }
        }
    }

    static void confirmDownload(String id, String message) {
        ZWIMetadata metadata = Downloader.getMetadata(id);
        if(metadata == null) error("Couldn't find that article.");
        assert metadata != null;
        info("Title", metadata.getTitle());
        info("Description", truncate(metadata.getDescription()));
        info("From", metadata.getPublisher());
        info("License", metadata.getLicense());
        info("Language", metadata.getLanguageCode());
        confirm(message, true);
    }

    static void displayResult(ZWIDatabase.SearchResult result) {
        bold("enc://" + result.getID());
        info("Title", result.getTitle());
        info("Description", truncate(result.getDescription()));
        info("From", result.getPublisher());
        println();
    }

    static void showAggregatorInfo(AggregatorInfo info) {
        bold(info.getName());
        println(info.getDescription());
        info("Number of articles", info.getArticleCount());
        info("Database size", humanReadableByteCountSI(info.getDatabaseSize()));
    }

    static void error(String message) {
        red("Error: " + message);
        exit(1);
    }

    static void invalidArgs() {
        red("Error: Invalid argument.");
        printUsageAndExit(1);
    }

    static void printUsageAndExit(int code) {
        render("""
                    @|bold EncycloTorrent Client v1.0|@
                    Usage: enc [OPTIONS]
                    Options:
                        enc search <keywords>                                  Search the Encyclosphere! Queries containing spaces must be in quotes.
                    
                        enc upload <file>                                      Upload an article to the Encyclosphere
                        enc upload <file> <aggregator URL>                     Upload an article to an aggregator
                        
                        enc info enc://<ID>                                    Get information about an article
                        enc info raw enc://<ID>                                Get the raw metadata of an article
                        
                        enc download enc://<ID>                                Download an article from the Encyclosphere
                        enc download torrent enc://<ID>                        Download a torrent of an article from the Encyclosphere
                        enc download withTorrent <torrent>                     Download an article using a torrent file
                        enc download ... <directory>                           Add a directory to the end of a download command to put the downloaded file in that directory
                      
                        enc pull <aggregator URL> <database dir>               Sync a folder with an aggregator's database (similar to git pull)
                        enc sync <aggregator URL> <database dir>               Sync a folder with an aggregator's database every 5 minutes
                      
                        enc ... verbose                                        Add "verbose" to the end of a command to print debug information
                        enc reset                                              Delete the .encyclotorrent folder, resetting all settings
                        enc help                                               Print this usage screen
                      
                    Password commands:
                        enc pass add <aggregator URL> <publisher> <password>   Add a password. Aggregators may require a password to upload articles from some publishers.
                        enc pass get <aggregator URL> <publisher>              Get a password
                        enc pass remove <aggregator URL> <publisher>           Remove a password
                        enc pass list <aggregator URL>                         Get a list of passwords for the given aggregator
                        enc pass list                                          Get a list of all passwords
                        enc pass clear                                         Clear the list of passwords
                      
                    Aggregator commands:
                        enc aggregator info <aggregator URL>                   Get information about an aggregator
                        enc aggregator add <aggregator URL>                    Add an aggregator to the list of aggregators
                        enc aggregator list                                    Get a list of all known aggregators
                        enc aggregator remove <aggregator URL>                 Remove an aggregator from the list of aggregators
                        enc aggregator list reset                              Reset the list of aggregators
                        enc aggregator discover                                Discover more aggregators using a DHT-like algorithm
                        
                        enc aggregator create <directory>                      Create an aggregator in the specified directory
                        enc aggregator create withDatabase <database dir>      Create an aggregator using an existing database
                        enc aggregator start <directory>                       Start an aggregator in the specified directory
                        
                    Database commands:
                        enc database <database dir> search <keywords>          Search a database. Queries containing spaces must be in quotes.
                        enc database <database dir> add <file>                 Add a file to a database
                        enc database <database dir> trash enc://<ID>           Move the article with the given ID to the trash in a database
                        enc database <database dir> emptyTrash                 Empty the trash of a database
                        enc database <database dir> buildIndexes               Update the index files and Lucene index of a database
                     
                    For more information, check out the wiki: @|bold https://gitlab.com/ks_found/EncycloTorrent/-/wikis/home|@
                    """);
        // TODO Add commands: enc database <database dir> restore enc://<ID> (restore file from trash), enc search <aggregator URL> <keywords> (search an aggregator)
        exit(code);
    }

    static void exit(int code) {
        System.exit(code);
    }

    static String truncate(String str) {
        if(str.length() > 100) return str.substring(0, 100) + "...";
        else return str;
    }

    static boolean isAggregatorFolder(Path folder) {
        return Files.isDirectory(folder)
                && Files.isDirectory(folder.resolve("db"))
                && Files.exists(folder.resolve("aggregator.json"));
    }

    // https://stackoverflow.com/a/3758880/5905216
    static String humanReadableByteCountSI(long bytes) {
        if(-1000 < bytes && bytes < 1000) {
            return bytes + " B";
        }
        //noinspection SpellCheckingInspection
        CharacterIterator ci = new StringCharacterIterator("KMGTPE");
        while(bytes <= -999_950 || bytes >= 999_950) {
            bytes /= 1000;
            ci.next();
        }
        return String.format("%.1f %cB", bytes / 1000.0, ci.current());
    }

}
