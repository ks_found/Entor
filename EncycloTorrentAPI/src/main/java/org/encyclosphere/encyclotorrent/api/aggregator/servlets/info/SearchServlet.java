package org.encyclosphere.encyclotorrent.api.aggregator.servlets.info;

import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.apache.lucene.queryparser.classic.ParseException;
import org.encyclosphere.encyclotorrent.api.aggregator.Aggregator;
import org.encyclosphere.encyclotorrent.api.aggregator.Aggregators;
import org.encyclosphere.encyclotorrent.api.misc.Utilities;
import org.encyclosphere.encyclotorrent.api.zwi.ZWIDatabase;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

public class SearchServlet extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String keywords = request.getParameter("q");
        if(keywords == null) {
            Utilities.sendError(response, 400, "Missing \"q\" query parameter");
            return;
        }

        String numResultsStr = request.getParameter("numResults");
        int numResults = 150;
        if(numResultsStr != null) {
            try {
                numResults = Integer.parseInt(numResultsStr);
            } catch(NumberFormatException e) {
                Utilities.sendError(response, 400, "\"numResults\" must be a number");
            }
        }

        String fields = request.getParameter("fields");
        String excludedPublishers = request.getParameter("excludedPublishers");
        String includedPublishers = request.getParameter("includedPublishers");

        Aggregator aggregator = Aggregators.getLocalAggregator(request.getLocalPort());
        assert aggregator != null;

        try {
            ArrayList<ZWIDatabase.SearchResult> results =
                    aggregator.getDatabase().search(keywords, numResults, (fields == null ? ZWIDatabase.SEARCH_ALL_FIELDS : fields), excludedPublishers, includedPublishers);
            JSONObject resultsObject = new JSONObject();
            JSONArray resultsArray = new JSONArray();
            for(ZWIDatabase.SearchResult result : results) resultsArray.put(result.toJSONObject());
            resultsObject.put("results", resultsArray);

            response.setContentType("application/json");
            response.getWriter().print(resultsObject);
        } catch(IllegalArgumentException e) {
            Utilities.sendError(response, 400, "Cannot specify both excludedPublishers and includedPublishers");
        } catch(IllegalStateException e) {
            Utilities.sendError(response, 400, "No publishers selected");
        } catch(ParseException e) {
            Utilities.sendError(response, 400, "Invalid query");
        }
    }

}
