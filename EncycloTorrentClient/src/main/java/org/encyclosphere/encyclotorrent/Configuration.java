package org.encyclosphere.encyclotorrent;

import org.encyclosphere.encyclotorrent.api.aggregator.Aggregators;
import org.encyclosphere.encyclotorrent.api.misc.Utilities;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

public class Configuration {

    public static final Path CONFIG_DIR = Path.of(System.getProperty("user.home"), ".encyclotorrent"),
                            CONFIG_FILE = CONFIG_DIR.resolve("config.json"),
                   AGGREGATOR_LIST_FILE = CONFIG_DIR.resolve("remoteAggregators.json");

    private static final ArrayList<PasswordEntry> passwords = new ArrayList<>();

    public record PasswordEntry(String aggregatorURL, String publisher, String password) {}

    public static void load() throws IOException {
        if(Files.notExists(AGGREGATOR_LIST_FILE)) Files.writeString(AGGREGATOR_LIST_FILE, "[]", StandardOpenOption.CREATE_NEW);
        if(Files.notExists(CONFIG_FILE)) Files.writeString(CONFIG_FILE, "{\"passwords\": []}", StandardOpenOption.CREATE_NEW);
        JSONArray passwordsArray = new JSONObject(Files.readString(CONFIG_FILE)).getJSONArray("passwords");
        for(int i = 0; i < passwordsArray.length(); i++) {
            JSONObject passwordEntry = passwordsArray.getJSONObject(i);
            passwords.add(new PasswordEntry(passwordEntry.getString("aggregatorURL"), passwordEntry.getString("publisher"), passwordEntry.getString("password")));
        }
        JSONArray aggregatorListArray = new JSONArray(Files.readString(AGGREGATOR_LIST_FILE));
        for(int i = 0; i < aggregatorListArray.length(); i++) {
            Aggregators.registerAggregator(aggregatorListArray.getString(i));
        }
    }

    public static void save() throws IOException {
        JSONArray passwordsArray = new JSONArray();
        for(PasswordEntry password : passwords) {
            JSONObject object = Objects.requireNonNull(Utilities.createOrderedJSONObject());
            object.put("aggregatorURL", password.aggregatorURL);
            object.put("publisher", password.publisher);
            object.put("password", password.password);
            passwordsArray.put(object);
        }
        JSONObject configObject = new JSONObject();
        configObject.put("passwords", passwordsArray);
        Files.writeString(CONFIG_FILE, configObject.toString(4));

        Files.writeString(AGGREGATOR_LIST_FILE, new JSONArray(Aggregators.getAggregators()).toString());
    }

    public static ArrayList<PasswordEntry> getPasswords() {
        return passwords;
    }

    public static HashMap<String, String> getPasswords(String publisher) {
        HashMap<String, String> passwordMap = new HashMap<>();
        for(PasswordEntry entry : passwords) {
            if(entry.publisher().equals(publisher)) passwordMap.put(entry.aggregatorURL(), entry.password());
        }
        return passwordMap;
    }

    public static void addPassword(PasswordEntry entry) {
        PasswordEntry existingEntry = getPassword(entry.aggregatorURL(), entry.publisher());
        if(existingEntry != null) passwords.remove(existingEntry);
        passwords.add(entry);
    }

    public static void removePassword(PasswordEntry entry) {
        passwords.remove(entry);
    }

    public static PasswordEntry getPassword(String aggregatorURL, String publisher) {
        for(Configuration.PasswordEntry entry : Configuration.getPasswords()) {
            if(entry.aggregatorURL().equals(aggregatorURL) && entry.publisher().equals(publisher)) return entry;
        }
        return null;
    }

}
