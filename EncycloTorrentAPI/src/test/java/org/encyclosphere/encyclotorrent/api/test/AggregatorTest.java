package org.encyclosphere.encyclotorrent.api.test;

import org.encyclosphere.encyclotorrent.api.Downloader;
import org.encyclosphere.encyclotorrent.api.Uploader;
import org.encyclosphere.encyclotorrent.api.aggregator.Aggregator;
import org.encyclosphere.encyclotorrent.api.index.IndexEntry;
import org.encyclosphere.encyclotorrent.api.index.IndexFile;
import org.encyclosphere.encyclotorrent.api.misc.Utilities;
import org.encyclosphere.encyclotorrent.api.zwi.ZWITorrent;
import org.encyclosphere.encyclotorrent.api.zwi.ZWIDatabase;
import org.encyclosphere.encyclotorrent.api.zwi.ZWIFile;
import org.junit.jupiter.api.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;

import static org.encyclosphere.encyclotorrent.api.misc.Utilities.isSameFile;
import static org.encyclosphere.encyclotorrent.api.test.TestUtilities.*;
import static org.junit.jupiter.api.Assertions.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class AggregatorTest {

    static ZWIDatabase firstDatabase, secondDatabase, thirdDatabase;

    static Aggregator firstAggregator, secondAggregator, thirdAggregator;

    @BeforeAll
    static void createAggregators() throws Exception {

        // Create some test databases
        firstDatabase = new ZWIDatabase(Path.of("testTemp/databases/firstDatabase"));
        secondDatabase = new ZWIDatabase(Path.of("testTemp/databases/secondDatabase"));
        thirdDatabase = new ZWIDatabase(Path.of("testTemp/databases/thirdDatabase"));

        // Create some test aggregators TODO
        //firstAggregator = new Aggregator("0.0.0.0", 22308, firstDatabase);
        //secondAggregator = new Aggregator("0.0.0.0", 22309, secondDatabase);
        //thirdAggregator = new Aggregator("0.0.0.0", 22310, thirdDatabase);

        // Set the password for the publisher "example"
        firstAggregator.getConfig().addPassword("example", "password");
        secondAggregator.getConfig().addPassword("example", "password");
        thirdAggregator.getConfig().addPassword("example", "password");

        firstAggregator.start();
        secondAggregator.start();
        thirdAggregator.start();
    }

    @Test
    @Order(1)
    public void testFileUpload() throws IOException {

        // Create some test files
        ZWIFile firstTestFile = new ZWIFile(FIRST_ZWI_PATH);
        ZWIFile secondTestFile = new ZWIFile(SECOND_ZWI_PATH);
        ZWIFile thirdTestFile = new ZWIFile(THIRD_ZWI_PATH);

        // Asserts that uploading with the wrong password or no password doesn't work
        assertFalse(Uploader.uploadToAggregator(firstTestFile, "http://0.0.0.0:22308", "wrongPassword"));
        assertFalse(Uploader.uploadToAggregator(firstTestFile, "http://0.0.0.0:22308"));


        // Upload the test files to the first aggregator
        assertTrue(Uploader.uploadToAggregator(firstTestFile, "http://0.0.0.0:22308", "password"));
        assertTrue(Uploader.uploadToAggregator(secondTestFile, "http://0.0.0.0:22308", "password"));
        assertTrue(Uploader.uploadToAggregator(thirdTestFile, "http://0.0.0.0:22308", "password"));

        // Upload the test files to the second aggregator
        assertTrue(Uploader.uploadToAggregator(firstTestFile, "http://0.0.0.0:22309", "password"));
        assertTrue(Uploader.uploadToAggregator(secondTestFile, "http://0.0.0.0:22309", "password"));
        assertTrue(Uploader.uploadToAggregator(thirdTestFile, "http://0.0.0.0:22309", "password"));

        // Upload the test files to the third aggregator
        assertTrue(Uploader.uploadToAggregator(firstTestFile, "http://0.0.0.0:22310", "password"));
        assertTrue(Uploader.uploadToAggregator(secondTestFile, "http://0.0.0.0:22310", "password"));
        assertTrue(Uploader.uploadToAggregator(thirdTestFile, "http://0.0.0.0:22310", "password"));

        // TODO Add some tests for uploading without a password

        // Asserts that the aggregators' temporary folders are empty
        assertTrue(Utilities.listFiles(Path.of("aggregatorTemp")).isEmpty());

        // Asserts that the aggregators' databases contain the expected files
        testDatabase(firstDatabase);
        testDatabase(secondDatabase);
        testDatabase(thirdDatabase);
    }

    static void testDatabase(ZWIDatabase database) throws IOException {
        for(Path databasePath : Utilities.listFiles(database.getRoot())) {
            assertEquals("en", databasePath.getFileName().toString());

            assertTrue(Files.exists(databasePath.resolve("index")));
            assertTrue(Files.exists(databasePath.resolve("zwi")));

            IndexFile indexFile = new IndexFile(databasePath.resolve("index").resolve("example.csv.gz"));

            for(String entryStr : indexFile.getEntryStrings()) {
                IndexEntry entry = new IndexEntry(entryStr);
                assertTrue(Arrays.asList("First_test_article.zwi", "Second_test_article.zwi", "Third_test_article.zwi").contains(entry.getFilename()));
                switch(entry.getFilename()) {
                    case "First_test_article.zwi" -> {
                        assertEquals("ZXhhbXBsZS5jb20vd2lraS9UZXN0X2FydGljbGU=", entry.getID());
                        assertEquals("Test_article", entry.getTitle());
                        assertEquals(4351, entry.getFileSize());
                        assertEquals(1644624000, entry.getTimestamp());
                    }
                    case "Second_test_article.zwi" -> {
                        assertEquals("ZXhhbXBsZS5jb20vd2lraS9TZWNvbmRfdGVzdF9hcnRpY2xl", entry.getID());
                        assertEquals("Second_test_article", entry.getTitle());
                        assertEquals(4298, entry.getFileSize());
                        assertEquals(1644624000, entry.getTimestamp());
                    }
                    case "Third_test_article.zwi" -> {
                        assertEquals("ZXhhbXBsZS5jb20vd2lraS9UaGlyZF90ZXN0X2FydGljbGU=", entry.getID());
                        assertEquals("Third_test_article", entry.getTitle());
                        assertEquals(4299, entry.getFileSize());
                        assertEquals(1644624000, entry.getTimestamp());
                    }
                }
            }

            List<String> filesInZWIFolder = Files.walk(databasePath.resolve("zwi").resolve("example"))
                    .map(path -> path.getFileName().toString()).toList();
            assertTrue(filesInZWIFolder.contains("First_test_article.zwi"));
            assertTrue(filesInZWIFolder.contains("Second_test_article.zwi"));
            assertTrue(filesInZWIFolder.contains("Third_test_article.zwi"));
        }
    }

    @Test
    @Order(2)
    // TODO Fix this
    public void testFileDownload() throws Exception {

        // Download the test files from the first aggregator
        ZWIFile firstTestFile = Downloader.downloadZWIFile("http://0.0.0.0:22308", "ZXhhbXBsZS5jb20vd2lraS9UZXN0X2FydGljbGU=", Path.of("testTemp/download/firstAggregator"));
        ZWIFile secondTestFile = Downloader.downloadZWIFile("http://0.0.0.0:22308", "ZXhhbXBsZS5jb20vd2lraS9TZWNvbmRfdGVzdF9hcnRpY2xl", Path.of("testTemp/download/firstAggregator"), false);
        ZWIFile thirdTestFile = Downloader.downloadZWIFile("http://0.0.0.0:22308", "ZXhhbXBsZS5jb20vd2lraS9UaGlyZF90ZXN0X2FydGljbGU=", Path.of("testTemp/download/firstAggregator"), false);

        // Download the test files from the second aggregator
        ZWIFile fourthTestFile = Downloader.downloadZWIFile("http://0.0.0.0:22309", "ZXhhbXBsZS5jb20vd2lraS9UZXN0X2FydGljbGU=", Path.of("testTemp/download/secondAggregator"), false, true);
        ZWIFile fifthTestFile = Downloader.downloadZWIFile("http://0.0.0.0:22309", "ZXhhbXBsZS5jb20vd2lraS9TZWNvbmRfdGVzdF9hcnRpY2xl", Path.of("testTemp/download/secondAggregator"), false, true);
        ZWIFile sixthTestFile = Downloader.downloadZWIFile("http://0.0.0.0:22309", "ZXhhbXBsZS5jb20vd2lraS9UaGlyZF90ZXN0X2FydGljbGU=", Path.of("testTemp/download/secondAggregator"), false, true);

        // Download the test files from the third aggregator
        ZWIFile seventhTestFile = Downloader.downloadZWIFile("http://0.0.0.0:22310", "ZXhhbXBsZS5jb20vd2lraS9UZXN0X2FydGljbGU=", Path.of("testTemp/download/thirdAggregator"), false, true);
        ZWIFile eighthTestFile = Downloader.downloadZWIFile("http://0.0.0.0:22310", "ZXhhbXBsZS5jb20vd2lraS9TZWNvbmRfdGVzdF9hcnRpY2xl", Path.of("testTemp/download/thirdAggregator"), false, true);
        ZWIFile ninthTestFile = Downloader.downloadZWIFile("http://0.0.0.0:22310", "ZXhhbXBsZS5jb20vd2lraS9UaGlyZF90ZXN0X2FydGljbGU=", Path.of("testTemp/download/thirdAggregator"), false, true);

        // Download the test files from a random aggregator
        ZWIFile tenthTestFile = Downloader.downloadZWIFile("ZXhhbXBsZS5jb20vd2lraS9UZXN0X2FydGljbGU=", Path.of("testTemp/download/anyAggregator"), false, true);
        ZWIFile eleventhTestFile = Downloader.downloadZWIFile("ZXhhbXBsZS5jb20vd2lraS9TZWNvbmRfdGVzdF9hcnRpY2xl", Path.of("testTemp/download/anyAggregator"), false, true);
        ZWIFile twelfthTestFile = Downloader.downloadZWIFile("ZXhhbXBsZS5jb20vd2lraS9UaGlyZF90ZXN0X2FydGljbGU=", Path.of("testTemp/download/anyAggregator"), false, true);


        // Assert that the test files exist (aren't null)
        assertNotNull(firstTestFile);
        assertNotNull(secondTestFile);
        assertNotNull(thirdTestFile);

        assertNotNull(fourthTestFile);
        assertNotNull(fifthTestFile);
        assertNotNull(sixthTestFile);

        assertNotNull(seventhTestFile);
        assertNotNull(eighthTestFile);
        assertNotNull(ninthTestFile);

        assertNotNull(tenthTestFile);
        assertNotNull(eleventhTestFile);
        assertNotNull(twelfthTestFile);


        // Assert that the files were downloaded properly by comparing hashes
        assertTrue(isSameFile(firstTestFile.getLocation(), FIRST_ZWI_PATH));
        assertTrue(isSameFile(secondTestFile.getLocation(), SECOND_ZWI_PATH));
        assertTrue(isSameFile(thirdTestFile.getLocation(), THIRD_ZWI_PATH));

        assertTrue(isSameFile(fourthTestFile.getLocation(), FIRST_ZWI_PATH));
        assertTrue(isSameFile(fifthTestFile.getLocation(), SECOND_ZWI_PATH));
        assertTrue(isSameFile(sixthTestFile.getLocation(), THIRD_ZWI_PATH));

        assertTrue(isSameFile(seventhTestFile.getLocation(), FIRST_ZWI_PATH));
        assertTrue(isSameFile(eighthTestFile.getLocation(), SECOND_ZWI_PATH));
        assertTrue(isSameFile(ninthTestFile.getLocation(), THIRD_ZWI_PATH));

        assertTrue(isSameFile(tenthTestFile.getLocation(), FIRST_ZWI_PATH));
        assertTrue(isSameFile(eleventhTestFile.getLocation(), SECOND_ZWI_PATH));
        assertTrue(isSameFile(twelfthTestFile.getLocation(), THIRD_ZWI_PATH));
    }

    @Test
    @Order(3)
    public void testTorrentDownload() throws IOException {

        // Download torrents of the test files from the first aggregator
        ZWITorrent firstTestFile = Downloader.downloadTorrent("http://0.0.0.0:22308", "ZXhhbXBsZS5jb20vd2lraS9UZXN0X2FydGljbGU=");
        ZWITorrent secondTestFile = Downloader.downloadTorrent("http://0.0.0.0:22308", "ZXhhbXBsZS5jb20vd2lraS9TZWNvbmRfdGVzdF9hcnRpY2xl");
        ZWITorrent thirdTestFile = Downloader.downloadTorrent("http://0.0.0.0:22308", "ZXhhbXBsZS5jb20vd2lraS9UaGlyZF90ZXN0X2FydGljbGU=");

        // Download torrents of the test files from the second aggregator
        ZWITorrent fourthTestFile = Downloader.downloadTorrent("http://0.0.0.0:22309", "ZXhhbXBsZS5jb20vd2lraS9UZXN0X2FydGljbGU=");
        ZWITorrent fifthTestFile = Downloader.downloadTorrent("http://0.0.0.0:22309", "ZXhhbXBsZS5jb20vd2lraS9TZWNvbmRfdGVzdF9hcnRpY2xl");
        ZWITorrent sixthTestFile = Downloader.downloadTorrent("http://0.0.0.0:22309", "ZXhhbXBsZS5jb20vd2lraS9UaGlyZF90ZXN0X2FydGljbGU=");

        // Download torrents of the test files from the third aggregator
        ZWITorrent seventhTestFile = Downloader.downloadTorrent("http://0.0.0.0:22310", "ZXhhbXBsZS5jb20vd2lraS9UZXN0X2FydGljbGU=");
        ZWITorrent eighthTestFile = Downloader.downloadTorrent("http://0.0.0.0:22310", "ZXhhbXBsZS5jb20vd2lraS9TZWNvbmRfdGVzdF9hcnRpY2xl");
        ZWITorrent ninthTestFile = Downloader.downloadTorrent("http://0.0.0.0:22310", "ZXhhbXBsZS5jb20vd2lraS9UaGlyZF90ZXN0X2FydGljbGU=");

        // Download torrents of the test files from a random aggregator
        ZWITorrent tenthTestFile = Downloader.downloadTorrent("ZXhhbXBsZS5jb20vd2lraS9UZXN0X2FydGljbGU=");
        ZWITorrent eleventhTestFile = Downloader.downloadTorrent("ZXhhbXBsZS5jb20vd2lraS9TZWNvbmRfdGVzdF9hcnRpY2xl");
        ZWITorrent twelfthTestFile = Downloader.downloadTorrent("ZXhhbXBsZS5jb20vd2lraS9UaGlyZF90ZXN0X2FydGljbGU=");


        // Assert that the test torrents exist (aren't null)
        assertNotNull(firstTestFile);
        assertNotNull(secondTestFile);
        assertNotNull(thirdTestFile);

        assertNotNull(fourthTestFile);
        assertNotNull(fifthTestFile);
        assertNotNull(sixthTestFile);

        assertNotNull(seventhTestFile);
        assertNotNull(eighthTestFile);
        assertNotNull(ninthTestFile);

        assertNotNull(tenthTestFile);
        assertNotNull(eleventhTestFile);
        assertNotNull(twelfthTestFile);


        // Assert that the torrents were downloaded properly by comparing hashes
        assertTrue(hashesMatch(firstTestFile.getTorrentBytes(), FIRST_TORRENT_PATH));
        assertTrue(hashesMatch(secondTestFile.getTorrentBytes(), SECOND_TORRENT_PATH));
        assertTrue(hashesMatch(thirdTestFile.getTorrentBytes(), THIRD_TORRENT_PATH));

        assertTrue(hashesMatch(fourthTestFile.getTorrentBytes(), FIRST_TORRENT_PATH));
        assertTrue(hashesMatch(fifthTestFile.getTorrentBytes(), SECOND_TORRENT_PATH));
        assertTrue(hashesMatch(sixthTestFile.getTorrentBytes(), THIRD_TORRENT_PATH));

        assertTrue(hashesMatch(seventhTestFile.getTorrentBytes(), FIRST_TORRENT_PATH));
        assertTrue(hashesMatch(eighthTestFile.getTorrentBytes(), SECOND_TORRENT_PATH));
        assertTrue(hashesMatch(ninthTestFile.getTorrentBytes(), THIRD_TORRENT_PATH));

        assertTrue(hashesMatch(tenthTestFile.getTorrentBytes(), FIRST_TORRENT_PATH));
        assertTrue(hashesMatch(eleventhTestFile.getTorrentBytes(), SECOND_TORRENT_PATH));
        assertTrue(hashesMatch(twelfthTestFile.getTorrentBytes(), THIRD_TORRENT_PATH));
    }

    @Test
    @Order(4)
    public void testDatabaseSync() throws IOException {

        // Create some more test databases
        ZWIDatabase firstTestDatabase = new ZWIDatabase(Path.of("testTemp/sync/firstAggregator"));
        ZWIDatabase secondTestDatabase = new ZWIDatabase(Path.of("testTemp/sync/secondAggregator"));
        ZWIDatabase thirdTestDatabase = new ZWIDatabase(Path.of("testTemp/sync/thirdAggregator"));

        // Sync the first test database with the first aggregator,
        // the second test database with the second aggregator, etc.
        firstTestDatabase.syncWith("http://0.0.0.0:22308", false);
        secondTestDatabase.syncWith("http://0.0.0.0:22309", false);
        thirdTestDatabase.syncWith("http://0.0.0.0:22310", false);

        // Test the downloaded databases
        testDatabase(firstTestDatabase);
        testDatabase(secondTestDatabase);
        testDatabase(thirdTestDatabase);

        // Remove the first test article from the first aggregator's database,
        // the second test article from the second aggregator's database, etc.
        firstDatabase.deleteFile(firstDatabase.getZWIFile("ZXhhbXBsZS5jb20vd2lraS9UZXN0X2FydGljbGU="));
        secondDatabase.deleteFile(secondDatabase.getZWIFile("ZXhhbXBsZS5jb20vd2lraS9TZWNvbmRfdGVzdF9hcnRpY2xl"));
        thirdDatabase.deleteFile(thirdDatabase.getZWIFile("ZXhhbXBsZS5jb20vd2lraS9UaGlyZF90ZXN0X2FydGljbGU="));

        // Sync the changes
        firstTestDatabase.syncWith("http://0.0.0.0:22308", false);
        secondTestDatabase.syncWith("http://0.0.0.0:22309", false);
        thirdTestDatabase.syncWith("http://0.0.0.0:22310", false);

        // Test if the changes were synced correctly
        // TODO Create a method to simplify this part of the code
        for(Path firstTestDatabasePath : Utilities.listFiles(firstTestDatabase.getRoot())) {
            assertEquals("en", firstTestDatabasePath.getFileName().toString());

            assertTrue(Files.exists(firstTestDatabasePath.resolve("index")));
            assertTrue(Files.exists(firstTestDatabasePath.resolve("zwi")));
            assertTrue(Files.exists(firstTestDatabasePath.resolve("trash")));

            IndexFile indexFile = new IndexFile(firstTestDatabasePath.resolve("index").resolve("example.csv.gz"));

            for(String entry : indexFile.getEntryStrings()) {
                assertTrue(entry.endsWith(",\"Second_test_article.zwi\",ZXhhbXBsZS5jb20vd2lraS9TZWNvbmRfdGVzdF9hcnRpY2xl,\"Second_test_article\",4298,1644624000")
                        || entry.endsWith(",\"Third_test_article.zwi\",ZXhhbXBsZS5jb20vd2lraS9UaGlyZF90ZXN0X2FydGljbGU=,\"Third_test_article\",4299,1644624000"));
            }

            List<String> filesInZWIFolder = Files.walk(firstTestDatabasePath.resolve("zwi").resolve("example"))
                    .map(path -> path.getFileName().toString()).toList();
            assertFalse(filesInZWIFolder.contains("First_test_article.zwi"));
            assertTrue(filesInZWIFolder.contains("Second_test_article.zwi"));
            assertTrue(filesInZWIFolder.contains("Third_test_article.zwi"));

            Path trash = firstTestDatabasePath.resolve("trash");
            List<Path> filesInTrash = Files.walk(trash).filter(path -> !path.equals(trash)).toList();
            assertEquals(1, filesInTrash.size());
            assertEquals("ZXhhbXBsZS5jb20vd2lraS9UZXN0X2FydGljbGU=@1644624000.zwi", filesInTrash.get(0).getFileName().toString());
            assertTrue(isSameFile(filesInTrash.get(0), FIRST_ZWI_PATH));
        }

        for(Path secondTestDatabasePath : Utilities.listFiles(secondTestDatabase.getRoot())) {
            assertEquals("en", secondTestDatabasePath.getFileName().toString());

            assertTrue(Files.exists(secondTestDatabasePath.resolve("index")));
            assertTrue(Files.exists(secondTestDatabasePath.resolve("zwi")));
            assertTrue(Files.exists(secondTestDatabasePath.resolve("trash")));

            IndexFile indexFile = new IndexFile(secondTestDatabasePath.resolve("index").resolve("example.csv.gz"));

            for(String entry : indexFile.getEntryStrings()) {
                assertTrue(entry.endsWith(",\"First_test_article.zwi\",ZXhhbXBsZS5jb20vd2lraS9UZXN0X2FydGljbGU=,\"Test_article\",4351,1644624000")
                        || entry.endsWith("\"Third_test_article.zwi\",ZXhhbXBsZS5jb20vd2lraS9UaGlyZF90ZXN0X2FydGljbGU=,\"Third_test_article\",4299,1644624000"));
            }

            List<String> filesInZWIFolder = Files.walk(secondTestDatabasePath.resolve("zwi").resolve("example"))
                    .map(path -> path.getFileName().toString()).toList();
            assertTrue(filesInZWIFolder.contains("First_test_article.zwi"));
            assertFalse(filesInZWIFolder.contains("Second_test_article.zwi"));
            assertTrue(filesInZWIFolder.contains("Third_test_article.zwi"));

            Path trash = secondTestDatabasePath.resolve("trash");
            List<Path> filesInTrash = Files.walk(trash).filter(path -> !path.equals(trash)).toList();
            assertEquals(1, filesInTrash.size());
            assertEquals("ZXhhbXBsZS5jb20vd2lraS9TZWNvbmRfdGVzdF9hcnRpY2xl@1644624000.zwi", filesInTrash.get(0).getFileName().toString());
            assertTrue(isSameFile(filesInTrash.get(0), SECOND_ZWI_PATH));
        }

        for(Path thirdTestDatabasePath : Utilities.listFiles(thirdTestDatabase.getRoot())) {
            assertEquals("en", thirdTestDatabasePath.getFileName().toString());

            assertTrue(Files.exists(thirdTestDatabasePath.resolve("index")));
            assertTrue(Files.exists(thirdTestDatabasePath.resolve("zwi")));
            assertTrue(Files.exists(thirdTestDatabasePath.resolve("trash")));

            IndexFile indexFile = new IndexFile(thirdTestDatabasePath.resolve("index").resolve("example.csv.gz"));

            for(String entry : indexFile.getEntryStrings()) {
                assertTrue(entry.endsWith(",\"First_test_article.zwi\",ZXhhbXBsZS5jb20vd2lraS9UZXN0X2FydGljbGU=,\"Test_article\",4351,1644624000")
                        || entry.endsWith(",\"Second_test_article.zwi\",ZXhhbXBsZS5jb20vd2lraS9TZWNvbmRfdGVzdF9hcnRpY2xl,\"Second_test_article\",4298,1644624000"));
            }

            List<String> filesInZWIFolder = Files.walk(thirdTestDatabasePath.resolve("zwi").resolve("example"))
                    .map(path -> path.getFileName().toString()).toList();
            assertTrue(filesInZWIFolder.contains("First_test_article.zwi"));
            assertTrue(filesInZWIFolder.contains("Second_test_article.zwi"));
            assertFalse(filesInZWIFolder.contains("Third_test_article.zwi"));

            Path trash = thirdTestDatabasePath.resolve("trash");
            List<Path> filesInTrash = Files.walk(trash).filter(path -> !path.equals(trash)).toList();
            assertEquals(1, filesInTrash.size());
            assertEquals("ZXhhbXBsZS5jb20vd2lraS9UaGlyZF90ZXN0X2FydGljbGU=@1644624000.zwi", filesInTrash.get(0).getFileName().toString());
            assertTrue(isSameFile(filesInTrash.get(0), THIRD_ZWI_PATH));
        }
    }

    @AfterAll
    static void cleanUp() throws Exception {
        firstAggregator.stop();
        secondAggregator.stop();
        thirdAggregator.stop();
        if(DELETE_TEMP_ON_TEST_FINISH) Utilities.deleteDirectory(Path.of("testTemp"));
    }

}
