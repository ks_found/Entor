package org.encyclosphere.encyclotorrent.api.test;

import org.encyclosphere.encyclotorrent.api.aggregator.Aggregators;
import org.encyclosphere.encyclotorrent.api.zwi.ZWITorrent;
import org.encyclosphere.encyclotorrent.api.zwi.ZWIFile;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ZWIFileTest {

    final ZWIFile testArticle = new ZWIFile(Path.of("src/test/resources/articles/First_test_article.zwi"));

    @BeforeAll
    static void setUp() {
        Aggregators.getAggregators().addAll(Arrays.asList("http://0.0.0.0:22308", "http://0.0.0.0:22309", "http://0.0.0.0:22310"));
    }

    @Test
    public void testID() throws IOException {
        assertEquals(testArticle.getID(), "ZXhhbXBsZS5jb20vd2lraS9UZXN0X2FydGljbGU=");
    }

    @Test
    public void testTorrent() throws IOException {
        ZWITorrent torrent = testArticle.getTorrent();
        assertEquals(torrent.getFilename(), "ZXhhbXBsZS5jb20vd2lraS9UZXN0X2FydGljbGU=@1644624000.torrent");
        //assertTrue(TestUtilities.hashesMatch(torrent.getContent(), TestUtilities.FIRST_TORRENT_PATH));
        // TODO Fix this
    }

}
