package org.encyclosphere.encyclotorrent.api.misc;

import jakarta.servlet.http.HttpServletResponse;
import me.tongfei.progressbar.ProgressBar;
import me.tongfei.progressbar.ProgressBarBuilder;
import me.tongfei.progressbar.ProgressBarStyle;
import net.lingala.zip4j.io.inputstream.ZipInputStream;
import net.lingala.zip4j.model.LocalFileHeader;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.lucene.index.IndexWriterConfig;
import org.encyclosphere.encyclotorrent.api.zwi.ZWIDatabase;
import org.encyclosphere.encyclotorrent.api.zwi.ZWIMetadata;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.lang.reflect.Field;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

/**
 * A utility class. The methods in this class are not intended to be used directly.
 */
public final class Utilities {

    public static final Logger logger = LoggerFactory.getLogger("EncycloTorrentAPI");

    private static boolean loggingEnabled = false;

    public static final HttpClient httpClient = HttpClientBuilder.create().build();



    // https://stackoverflow.com/a/35797158/5905216
    public static JSONObject createOrderedJSONObject() {
        try {
            JSONObject object = new JSONObject();
            Field map = object.getClass().getDeclaredField("map");
            map.setAccessible(true);
            map.set(object, new LinkedHashMap<>());
            map.setAccessible(false);
            return object;
        } catch(Exception e) {
            ExceptionHandler.getExceptionHandler().accept(e);
            return null;
        }
    }

    public static void log(String message) {
        if(loggingEnabled) logger.info(message);
    }

    public static void logException(String message, Exception e) {
        if(loggingEnabled) logger.error(message, e);
    }

    public static void setLoggingEnabled(boolean loggingEnabled) {
        Utilities.loggingEnabled = loggingEnabled;
    }

    private static final int TIMEOUT_MILLIS = 5000;

    public static final RequestConfig requestConfig = RequestConfig.custom()
            .setSocketTimeout(TIMEOUT_MILLIS)
            .setConnectTimeout(TIMEOUT_MILLIS)
            .setConnectionRequestTimeout(TIMEOUT_MILLIS)
            .build();

    public static HttpResponse makeRequest(HttpRequestBase request) throws IOException {
        request.setConfig(requestConfig);
        return httpClient.execute(request);
    }

    public static HttpResponse makeRequest(HttpRequestBase request, int timeout) throws IOException {
        request.setConfig(RequestConfig.custom()
                .setSocketTimeout(timeout)
                .setConnectTimeout(timeout)
                .setConnectionRequestTimeout(timeout)
                .build());
        return httpClient.execute(request);
    }

    public static void deleteDirectory(Path directory) throws IOException {
        if(Files.notExists(directory)) return;
        //noinspection ResultOfMethodCallIgnored
        Files.walk(directory)
                .sorted(Comparator.reverseOrder())
                .map(Path::toFile)
                .forEach(File::delete);
    }

    public static byte[] getFileContentsInZWI(byte[] zwiBytes, String pathInZip) throws IOException {
        ZipInputStream zwiStream = new ZipInputStream(new ByteArrayInputStream(zwiBytes));
        LocalFileHeader header;
        while((header = zwiStream.getNextEntry()) != null) {
            if(header.getFileName().equals(pathInZip)) return zwiStream.readAllBytes();
        }
        return null;
    }

    public static Path getPathInDatabase(ZWIDatabase database, ZWIMetadata metadata) {
        return database.getRoot()
                .resolve(metadata.getLanguageCode())
                .resolve("zwi")
                .resolve(metadata.getPublisher())
                .resolve(Utilities.getPartialPath(metadata.getID()))
                .resolve(metadata.getID() + ".zwi");
    }

    public static ProgressBar createProgressBar(String taskName, long initialMax) {
        return new ProgressBarBuilder()
                .setTaskName(taskName)
                .setInitialMax(initialMax)
                .setMaxRenderedLength(125)
                .setUpdateIntervalMillis(200)
                .setStyle(ProgressBarStyle.ASCII)
                .build();
    }

    public static List<Path> listFiles(Path path) throws IOException {
        return Files.list(path).toList();
    }

    public static byte[] readGzippedBytes(Path file) throws IOException {
        return gunzip(Files.readAllBytes(file));
    }

    public static void writeGzippedBytes(Path file, byte[] bytes) throws IOException {
        if(Files.notExists(file)) {
            Files.createDirectories(file.getParent());
            Files.createFile(file);
        }
        GZIPOutputStream gzipOutputStream = new GZIPOutputStream(new FileOutputStream(file.toFile()));
        gzipOutputStream.write(bytes);
        gzipOutputStream.close();
    }

    public static byte[] gunzip(byte[] bytes) throws IOException {
        return new GZIPInputStream(new ByteArrayInputStream(bytes)).readAllBytes();
    }

    public static void sendError(HttpServletResponse response, int code, String message) throws IOException {
        response.setStatus(code);
        response.setContentType("application/json");
        response.getWriter().println("{\"error\": " + code + ", \"message\": \"" + message + "\"}");
    }

    public static String getStackTrace(Throwable e) {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        e.printStackTrace(pw);
        return sw.toString();
    }

    public static String processID(String id) {
        id = id.strip();
        if(id.startsWith("enc://")) id = id.substring(6);
        return id;
    }

    public static IndexWriterConfig getIndexWriterConfig() {
        IndexWriterConfig config = new IndexWriterConfig();
        config.setCommitOnClose(true);
        return config;
    }

    static MessageDigest sha1Digest, sha256Digest;
    static {
        try {
            sha1Digest = MessageDigest.getInstance("SHA-1");
            sha256Digest = MessageDigest.getInstance("SHA-256");
        } catch(NoSuchAlgorithmException e) {
            ExceptionHandler.getExceptionHandler().accept(e);
        }
    }

    public static String getPartialPath(String id) {
        char firstChar = id.charAt(0), secondChar = id.charAt(1);
        return firstChar + "/" + firstChar + "" + secondChar;
    }

    // https://stackoverflow.com/a/4895572/5905216
    public static String getSHA1Hash(byte[] input) {
        return byteArrayToHexString(sha1Digest.digest(input));
    }

    // https://stackoverflow.com/a/4895572/5905216
    public static String getSHA256Hash(byte[] input) {
        return byteArrayToHexString(sha256Digest.digest(input));
    }

    public static String byteArrayToHexString(byte[] b) {
        StringBuilder result = new StringBuilder();
        for(byte value : b) {
            result.append(Integer.toString((value & 0xff) + 0x100, 16).substring(1));
        }
        return result.toString();
    }

    public static boolean isSameFile(Path one, Path other) throws IOException {
        return Utilities.getSHA1Hash(Files.readAllBytes(one)).equals(Utilities.getSHA1Hash(Files.readAllBytes(other)));
    }

    /**
     * Decodes the passed UTF-8 String using an algorithm that's compatible with
     * JavaScript's <code>decodeURIComponent</code> function. Returns
     * <code>null</code> if the String is <code>null</code>.
     *
     * @param s The UTF-8 encoded String to be decoded
     * @return the decoded String
     */
    public static String decodeURIComponent(String s) {
        if(s == null) return null;
        return URLDecoder.decode(s, StandardCharsets.UTF_8);
    }

    /**
     * Encodes the passed String as UTF-8 using an algorithm that's compatible
     * with JavaScript's <code>encodeURIComponent</code> function. Returns
     * <code>null</code> if the String is <code>null</code>.
     *
     * @param s The String to be encoded
     * @return the encoded String
     * @author John Topley
     * @see <a href="https://stackoverflow.com/a/611117/5905216">Source of method</a>
     */
    public static String encodeURIComponent(String s) {
        String result;
        result = URLEncoder.encode(s, StandardCharsets.UTF_8)
                .replaceAll("\\+", "%20")
                .replaceAll("%21", "!")
                .replaceAll("%27", "'")
                .replaceAll("%28", "(")
                .replaceAll("%29", ")")
                .replaceAll("%7E", "~");

        return result;
    }

}
