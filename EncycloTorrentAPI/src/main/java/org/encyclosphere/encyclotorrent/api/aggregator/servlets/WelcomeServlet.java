package org.encyclosphere.encyclotorrent.api.aggregator.servlets;

import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.encyclosphere.encyclotorrent.api.aggregator.Aggregator;
import org.encyclosphere.encyclotorrent.api.aggregator.Aggregators;
import org.encyclosphere.encyclotorrent.api.misc.ExceptionHandler;

import java.io.IOException;
import java.io.InputStream;

public class WelcomeServlet extends HttpServlet {

    private static final String welcomeHTML;

    static {
        String welcomeHTMLTemp;
        InputStream htmlStream = WelcomeServlet.class.getResourceAsStream("/aggregator/index.html");
        try {
            if(htmlStream == null) throw new IOException("Failed to read /aggregator/index.html");
            welcomeHTMLTemp = new String(htmlStream.readAllBytes());
        } catch(IOException | NullPointerException e) {
            welcomeHTMLTemp = null;
            ExceptionHandler.getExceptionHandler().accept(e);
        }
        welcomeHTML = welcomeHTMLTemp;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        Aggregator aggregator = Aggregators.getLocalAggregator(request.getLocalPort());
        assert aggregator != null;

        response.setContentType("text/html");
        response.getOutputStream().print(welcomeHTML.replace("${AGGREGATOR_URL}", aggregator.getURL()));
    }

}
