#!/bin/bash
# Based on https://coderwall.com/p/ssuaxa/how-to-make-a-jar-file-linux-executable
# and https://stackoverflow.com/a/7335524/5905216
MYSELF=`which "$0" 2>/dev/null`
[ $? -gt 0 -a -f "$0" ] && MYSELF="./$0"

function install_java {
    echo -ne "Java 17 is not installed. Would you like to install it? [y/N] "
    while true; do
        read response
        case "$response" in
            [yY])
                sudo apt install openjdk-17-jre
                sudo update-alternatives --set java /usr/lib/jvm/java-17-openjdk-amd64/bin/java
                exit 0
                ;;
            [nN]|"")
                exit 0
                ;;
        esac
    done
}

if type -p java > /dev/null; then
    java=java
elif [[ -n "$JAVA_HOME" ]] && [[ -x "$JAVA_HOME/bin/java" ]]; then
    java="$JAVA_HOME/bin/java"
else
    install_java
fi

if [[ "$java" ]]; then
    version=$("$java" -version 2>&1 | awk -F '"' '/version/ {print $2}' | cut -f1 -d ".")
    if [[ "$version" -lt 17 ]]; then
        install_java
    fi
fi

exec "$java" $java_args -jar $MYSELF "$@"
exit 1 
