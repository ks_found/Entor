package org.encyclosphere.encyclotorrent.api.aggregator.servlets.download;

import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import net.lingala.zip4j.ZipFile;
import net.lingala.zip4j.model.FileHeader;
import org.encyclosphere.encyclotorrent.api.aggregator.Aggregator;
import org.encyclosphere.encyclotorrent.api.aggregator.Aggregators;
import org.encyclosphere.encyclotorrent.api.misc.Utilities;
import org.encyclosphere.encyclotorrent.api.zwi.ZWIFile;

import java.io.IOException;
import java.io.InputStream;

public class ZWIMetadataServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String id = request.getParameter("id");
        if(id == null) {
            Utilities.sendError(response, 400, "Missing ID");
            return;
        }

        Aggregator aggregator = Aggregators.getLocalAggregator(request.getLocalPort());
        assert aggregator != null;

        ZWIFile file = aggregator.getDatabase().getZWIFile(id);
        if(file == null) {
            Utilities.sendError(response, 404, "Couldn't find an article with the given ID in the database");
            return;
        }

        ZipFile zwiFile = new ZipFile(file.getLocation().toAbsolutePath().toString());
        FileHeader header = zwiFile.getFileHeader("metadata.json");
        InputStream metadataStream = zwiFile.getInputStream(header);

        response.setContentType("application/json");
        response.getWriter().print(new String(metadataStream.readAllBytes()));
    }
}
